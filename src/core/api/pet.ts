import { Animal } from '../models/pet';

export async function getPetsByType(params: any) {
  const [, { type, limit }] = params.queryKey;
  const response = await fetch(`http://localhost:8080/api/v1/pet/?type=${type}&limit=${limit}`);
  if (!response.ok) {
    throw new Error("Problem fetching data");
  }
  const animals: Animal = await response.json();

  return animals;
}


export async function getPets(params: any) {
  const [, { limit }] = params.queryKey;
  const response = await fetch(`http://localhost:8080/api/v1/pet/?limit=${limit}`);
  if (!response.ok) {
    throw new Error("Problem fetching data");
  }
  const animals: Animal = await response.json();

  return animals;
}

