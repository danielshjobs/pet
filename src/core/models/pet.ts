export interface Pet {
    image: string;
    name: string;
    primaryBreed: string;
    gender: string;
    age: string;


}

export interface Animal {
    type: string;
    pet: Pet[];

}