import { FaHeart, FaCat, FaDog } from "react-icons/fa"


export const SidebarData=[
    {
        title: "Animals",
        path: "/",
        icon: <FaHeart />,
      },
      {
        title: "Dog",
        path: "/dog",
        icon: <FaDog />,
      },
      {
        title: "Cat",
        path: "/cat",
        icon: <FaCat />,
      }
]