
import React from "react";
import { Routes, Route } from "react-router-dom";
import Cat from '../pages/Cat'
import Dog from '../pages/Dog'
import Pets from '../pages/Pets'


const NavPage = () => {
  return (
    <React.Fragment>
      <section>
        <Routes>
        <Route path="/" element={<Pets />} />
        <Route path="/dog" element={<Dog />} />
        <Route path="/cat" element={<Cat />} />
        </Routes>
      </section>
      </React.Fragment>
  );
};

export default NavPage;