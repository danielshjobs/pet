import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Dog from './pages/Dog';
import Pet from './pages/Pets';
import Cat from './pages/Cat';
import { BrowserRouter } from 'react-router-dom';
import MainPage from './component/MainPage';


function App() {
  return (
    <React.Fragment>
      <BrowserRouter>
        <MainPage />
      </BrowserRouter>
  </React.Fragment>
  );
}

export default App;
