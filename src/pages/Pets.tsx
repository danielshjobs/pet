import React from "react";
import { Card, Col, Row } from "react-bootstrap";
import { useQuery } from "react-query";
import  { getPets }  from "../core/api/pet";
import { Animal } from "../core/models/pet";

 
export default function Pet() {
    const { status, error, data } = useQuery<Animal, Error>(
      ["animal", {limit: 4 }],
      getPets
    );
  
    if (status === "loading") {
      return <div>...</div>;
    }
    if (status === "error") {
      return <div>{error!.message}</div>;
    }
  
    return (
            <div className="text-black">
              <Row xs={1} md={4} className="g-4">
              {
                data?.pet.map((pet, index)=>{
                  return ( 
                        <Col  key={index}>
                          <Card style={{width: '18rem'}}>
                              <Card.Img variant="top" src={pet.image} />
                              <Card.Body>
                              <Card.Title>{pet.name}</Card.Title>
                              <Card.Text style={{fontSize: '15px'}}>
                                The pet is really {pet.age} of gender {pet.gender},
                                While the primary breed of him is : {pet.primaryBreed}
                              </Card.Text>
                            </Card.Body>
                          </Card>
                        </Col>
                       )
                })
          }
          </Row>
        </div>
      
    )
  }
