import React from "react";
import { Card, CardGroup } from "react-bootstrap";
import { useQuery } from "react-query";
import  { getPetsByType }  from "../core/api/pet";
import { Animal } from "../core/models/pet";


 
export default function Cat() {
  const { status, error, data } = useQuery<Animal, Error>(
    ["animal", { type: "cat", limit: 4 }],
    getPetsByType
  );

  if (status === "loading") {
    return <div>...</div>;
  }
  if (status === "error") {
    return <div>{error!.message}</div>;
  }

  return (
    <div className="text-black">
      <CardGroup>
        {
          data?.pet.map((pet, index)=>{
            return ( 
                    <Card style={{width: '18rem'}} key={index}>
                        <Card.Img variant="top" src={pet.image} />
                        <Card.Body>
                        <Card.Title >{pet.name}</Card.Title>
                        <Card.Text style={{fontSize: '15px' }} >
                          The cat is really {pet.age} of gender {pet.gender},
                          While the primary breed of him is : {pet.primaryBreed}
                        </Card.Text>
                      </Card.Body>
                    </Card>
            )
          })
        }
      </CardGroup>
    </div>
  )
}

