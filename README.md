# Pet micro front - Provided A pet front end application:


## Getting started

At a high level, Pet micro front goal is to present all our pet's data through a web app as a UX experience.

Pet micro front will deliver a smooth and comfortable experience to the customer through a special design that includes a sidebar and navigation, while the components are designed using popular libraries such as: (bootstrap, react Icon more...)


## What Pet micro front includes

- Pet micro front includes a side bar to present all our pages and managed the navigation between our components by react router dom.
- Pet micro front is based on popular layers by the most popular patterns such as :(core, models, Pages , shared component, error handler...)
- Pet micro front includes variety of artifacts such as: (react query, React router DOM, react bootstrap & cards, react TS..)
   

## To Install:

 - node.js  
 - recommended IDE intellij ultimate/ visual studio code

## Run the app:

- npm start.   

## Run test:

- npm test
 
## Run build:      

- npm run build
 

## To Deploy & build a docker image

- My recommendation is to base on SSR, then we would be able to rely on same deployment process of the backend.
- Spring boot can be used as infrastructure to SSR by maven plugin: (org.codehaus.mojo.exec-maven-plugin.)
- While we used by the step above we can deploy the react app by the steps below.

- To build docker image please run the command below -
     mvn jib:build -Djib.to.auth.username=$DOCKER_USER -Djib.to.auth.password=$DOCKER_PASSWORD -Djib.to.image.=$DOCKER_HOST 

- To deploy please assist by Helm 3 there is ready pet chart for your convenience in backend project, the command is: helm upgrade -i "Release" helm/Pet 

